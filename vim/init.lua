require('plugins')
require('dsn.globals')
require('dsn.treesitter')
require('dsn.nvimtree')
require('dsn.statusline')
require('dsn.prettier')
require('dsn.settings')
require('dsn.mappings') -- mappings, compe, lsp & telescope
